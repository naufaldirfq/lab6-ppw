from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
# from .views import *
from .models import Status
from .forms import Add_Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_lab6_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_using_index_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)

	def test_lab6_using_landing_page_template(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response, 'landing_page.html')

	def test_model_can_create_new_status(self):
		new_status = Status.objects.create(status = "hai")
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Add_Status_Form()
		self.assertIn('status', form.as_p())

	def test_lab6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/lab-6/', {'status': test})
		self.assertEqual(response_post.status_code, 200)
		response= Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


	def test_profile_url_is_exist(self):
		response = Client().get('/profil/')
		self.assertEqual(response.status_code, 200)

	def test_profile_using_profile_func(self):
		found = resolve('/profil/')
		self.assertEqual(found.func, profile)

	def test_profile_using_profile_template(self):
		response = Client().get('/profil/')
		self.assertTemplateUsed(response, 'profile.html')



# Create your tests here.
# class Lab6FunctionalTest(TestCase):

#     def setUp(self):
#     	chrome_options = Options()
#     	chrome_options.add_argument('--dns-prefetch-disable')
#     	chrome_options.add_argument('--no-sandbox')
#     	#chrome_options.add_argument('--headless')
#     	chrome_options.add_argument('disable-gpu')
#     	self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#     	super(Lab6FunctionalTest, self).setUp()
    	
#     def tearDown(self):
#     	self.selenium.quit()
#     	super(Lab6FunctionalTest, self).tearDown()

#     def test_input_todo(self):
#     	selenium = self.selenium
#     	selenium.get('https://naufaldi-lab6-ppwc.herokuapp.com/lab-6/')
#     	status = selenium.find_element_by_name('status')
#     	status.send_keys('coba coba')
#     	status.submit()
#     	time.sleep(3)
#     	self.assertIn('coba', selenium.page_source)

#     def test_layout_header(self):
#     	selenium = self.selenium
#     	selenium.get('https://naufaldi-lab6-ppwc.herokuapp.com/lab-6/')
#     	header_text = self.selenium.find_element_by_tag_name('h1').text
#     	time.sleep(3)
#     	self.assertIn('Halo, apa kabar?', header_text)
		
#     def test_layout_title(self):
#     	selenium = self.selenium
#     	selenium.get('https://naufaldi-lab6-ppwc.herokuapp.com/lab-6/')
#     	title_text = self.selenium.find_element_by_tag_name('th').text
#     	time.sleep(3)
#     	self.assertIn("Status", title_text)

#     def test_jumbotron_form(self):
#     	selenium = self.selenium
#     	selenium.get('https://naufaldi-lab6-ppwc.herokuapp.com/lab-6/')
#     	content = selenium.find_element_by_class_name('jumbotron').value_of_css_property('width')
#     	time.sleep(3)
#     	self.assertIn("500px", content)

#     def test_submit_button_form(self):
#     	selenium = self.selenium
#     	selenium.get('https://naufaldi-lab6-ppwc.herokuapp.com/lab-6/')
#     	content = selenium.find_element_by_class_name('btn-info ').value_of_css_property('color')
#     	time.sleep(3)
#     	self.assertIn('rgba(255, 255, 255, 1)', content)




if __name__ == '__main__': #
	unittest.main(warnings='ignore') #