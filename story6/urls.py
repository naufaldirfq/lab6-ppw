from django.conf.urls import url
from .views import index, profile

urlpatterns = [
	url('', index, name = 'index'),
	url('/profil/', profile, name = 'profile')
	# url('savestatus/', savestatus, name = 'savestatus'),	
]