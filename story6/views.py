from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Add_Status_Form
# Create your views here.
landing_page = {}

def index(request):
	status = Status.objects.all()
	landing_page['status'] = status
	landing_page['form'] = Add_Status_Form
	form = Add_Status_Form(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		landing_page['status'] = request.POST['status']
		status = Status(status = landing_page['status'])
		status.save()
		status = Status.objects.all()
		landing_page['status'] = status
	return render(request, 'landing_page.html', landing_page)

def profile(request):
	return render(request, 'profile.html')