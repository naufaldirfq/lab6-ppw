from django import forms

class Add_Status_Form(forms.Form):
	status_attrs = {
		'type' : 'text',
		'placeholder' : 'Whats on your mind?' 
	}

	status = forms.CharField(required = True, max_length = 144, widget = forms.TextInput(attrs = status_attrs))