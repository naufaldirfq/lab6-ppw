"""lab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls import url
import story6.urls as story6
from story6 import views
from story6.views import profile as pageprofile
from story6.views import *
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    # re_path('', views.index, name = 'index'),
    re_path('profil/', pageprofile, name = 'profil'),
    path('lab-6/', include(('story6.urls', 'story6'), namespace = "story6")),
    url(r'^$', RedirectView.as_view(url='lab-6/', permanent = True), name='$'),
]
